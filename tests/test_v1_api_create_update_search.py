import pytest
from moto import mock_dynamodb2
from falcon.testing import TestClient as FalconClient, Result
from freezegun import freeze_time
from isodate import parse_datetime

from subribbits import APP
from subribbits.v1.subribbit import Subribbit
from subribbits.v1.subscription import Subscription


@pytest.fixture(name="user_id")
def user_id_fixture() -> str:
    return "user123"


@pytest.fixture(name='client')
def client_fixture() -> FalconClient:
    with mock_dynamodb2():
        Subribbit.create_table(read_capacity_units=1, write_capacity_units=1)
        Subscription.create_table(read_capacity_units=1, write_capacity_units=1)
        yield FalconClient(APP)


@pytest.fixture(name='sub1')
def sub1_fixture() -> Subribbit:
    sub = Subribbit(
        name='sub1',
        creator='user1',
        created=parse_datetime("2019-01-01T00:00:00Z")
    )
    sub.save()
    return sub


@pytest.fixture(name='sub2')
def sub2_fixture(user_id: str) -> Subribbit:
    sub = Subribbit(
        name='sub2',
        creator='user1',
        created=parse_datetime("2019-01-02T00:00:00Z")
    )
    sub.save()
    return sub


@pytest.fixture(name='private')
def private_fixture() -> Subribbit:
    sub = Subribbit(
        name='private_sub',
        creator='user1',
        created=parse_datetime("2019-01-03T00:00:00Z"),
        private=True
    )
    sub.save()
    subscribe_to(sub, sub.creator)
    return sub


def user_env(user_id: str) -> dict:
    return {"awsgi.event": {"requestContext": {"authorizer": {"principalId": user_id}}}}


def subscribe_to(subribbit: Subribbit, user_id: str):
    sub = Subscription(subribbit=subribbit.name, user=user_id)
    sub.save()


##########
# Create #
##########


def test_create_unauthorized(client: FalconClient):
    response = client.simulate_post(
        '/subribbits/v1/r',
        json=dict(name='sub2')
    )
    assert response.status_code == 401


@freeze_time("2019-01-01T00:00:00Z")
def test_create(client: FalconClient):
    response = client.simulate_post(
        '/subribbits/v1/r',
        json=dict(name='sub2'),
        extras=user_env("user123")
    )

    assert response.status_code == 200
    assert response.json == dict(
        name='sub2',
        creator='user123',
        created='2019-01-01T00:00:00+00:00',
        private=False
    )


def test_creator_is_automatically_subscribed(client: FalconClient):
    client.simulate_post(
        '/subribbits/v1/r',
        json=dict(name='sub2'),
        extras=user_env("user123")
    )
    assert Subscription.is_subscribed("sub2", "user123") is True


def test_create_exists(client, sub1: Subribbit):
    response = client.simulate_post(
        '/subribbits/v1/r',
        json=dict(name=sub1.name),
        extras=user_env("user123")
    )
    assert response.status_code == 400


def test_create_invalid(client, user_id: str):
    response: Result = client.simulate_post(
        '/subribbits/v1/r',
        json=dict(),
        extras=user_env(user_id)
    )
    assert response.status_code == 400


def test_create_name_too_short(client: FalconClient, user_id: str):
    response: Result = client.simulate_post(
        '/subribbits/v1/r',
        json=dict(name="s"),
        extras=user_env(user_id)
    )

    assert response.status_code == 400


##########
# Delete #
##########


def test_delete_missing(client, user_id: str):
    response = client.simulate_delete(
        '/subribbits/v1/r/sub1',
        extras=user_env(user_id)
    )
    assert response.status_code == 404


def test_delete_am_creator(client, sub1: Subribbit, user_id: str):
    """Only the subribbits creator can delete a subribbit"""
    sub1.creator = user_id
    sub1.save()

    response: Result = client.simulate_delete(
        f'/subribbits/v1/r/{sub1.name}',
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json['name'] == sub1.name


def test_delete_not_creator(client, sub1: Subribbit, user_id: str):
    """Only the subribbits creator can delete a subribbit"""
    response: Result = client.simulate_delete(
        f'/subribbits/v1/r/{sub1.name}',
        extras=user_env(user_id)
    )

    assert response.status_code == 403


##########
# Search #
##########


def test_search_empty(client: FalconClient, user_id: str):
    response = client.simulate_get(
        "/subribbits/v1/r",
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == []


def test_search_none_found(client: FalconClient, user_id: str):
    response = client.simulate_get(
        "/subribbits/v1/r",
        params=dict(name=["sub1", "sub2"]),
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == []


def test_search_partial_found(client: FalconClient, sub1: Subribbit, user_id: str):
    response = client.simulate_get(
        "/subribbits/v1/r",
        params=dict(name=[sub1.name, "sub2"]),
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == [
        dict(
            name=sub1.name,
            creator=sub1.creator,
            created="2019-01-01T00:00:00+00:00",
            private=False
        )
    ]


def test_search_all_found(client: FalconClient, sub1: Subribbit, sub2: Subribbit, user_id: str):
    response = client.simulate_get(
        "/subribbits/v1/r",
        params=dict(name=[sub1.name, sub2.name]),
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == [
        dict(
            name=sub2.name,
            creator=sub2.creator,
            created="2019-01-02T00:00:00+00:00",
            private=False
        ),
        dict(
            name=sub1.name,
            creator=sub1.creator,
            created="2019-01-01T00:00:00+00:00",
            private=False
        )
    ]


def test_search_single(client: FalconClient, sub1: Subribbit, user_id: str):
    response = client.simulate_get(
        "/subribbits/v1/r",
        params=dict(name=[sub1.name]),
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == [
        dict(
            name=sub1.name,
            creator=sub1.creator,
            created="2019-01-01T00:00:00+00:00",
            private=False
        )
    ]


def test_search_private_not_allowed(client: FalconClient, private: Subribbit, user_id: str):
    response = client.simulate_get(
        "/subribbits/v1/r",
        params=dict(name=[private.name]),
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == []


def test_search_private_as_creator(client: FalconClient, private: Subribbit):
    response = client.simulate_get(
        "/subribbits/v1/r",
        params=dict(name=[private.name]),
        extras=user_env(private.creator)
    )

    print("toll")
    print(list(Subribbit.query("private_sub")))

    assert response.status_code == 200
    assert response.json == [
        dict(
            name=private.name,
            creator=private.creator,
            created="2019-01-03T00:00:00+00:00",
            private=True
        )
    ]


def test_search_private_as_subscriber(client: FalconClient, private: Subribbit, user_id: str):
    subscribe_to(private, user_id)

    response = client.simulate_get(
        "/subribbits/v1/r",
        params=dict(name=[private.name]),
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == [
        dict(
            name=private.name,
            creator=private.creator,
            created="2019-01-03T00:00:00+00:00",
            private=True
        )
    ]
