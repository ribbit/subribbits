import pytest
from falcon.testing import TestClient as FalconClient
from isodate import parse_datetime
from moto import mock_dynamodb2

from subribbits import APP
from subribbits.v1.subribbit import Subribbit
from subribbits.v1.subscription import Subscription


@pytest.fixture(name="user_id")
def user_id_fixture() -> str:
    return "user123"


@pytest.fixture(name='client')
def client_fixture() -> FalconClient:
    with mock_dynamodb2():
        Subribbit.create_table(read_capacity_units=1, write_capacity_units=1)
        Subscription.create_table(read_capacity_units=1, write_capacity_units=1)
        yield FalconClient(APP)


@pytest.fixture(name='sub1')
def sub1_fixture() -> Subribbit:
    sub = Subribbit(
        name='sub1',
        creator='user1',
        created=parse_datetime("2019-01-01T00:00:00Z")
    )
    sub.save()

    Subscription(subribbit=sub.name, user=sub.creator).save()

    return sub


@pytest.fixture(name='sub2')
def sub2_fixture(user_id: str) -> Subribbit:
    sub = Subribbit(
        name='sub2',
        creator='user1',
        created=parse_datetime("2019-01-02T00:00:00Z")
    )
    sub.save()

    Subscription(subribbit=sub.name, user=sub.creator).save()

    return sub


@pytest.fixture(name='private')
def private_fixture() -> Subribbit:
    sub = Subribbit(
        name='private_sub',
        creator='user1',
        created=parse_datetime("2019-01-03T00:00:00Z"),
        private=True
    )
    sub.save()

    Subscription(subribbit=sub.name, user=sub.creator).save()

    return sub


def user_env(user_id: str) -> dict:
    return {"awsgi.event": {"requestContext": {"authorizer": {"principalId": user_id}}}}


def subscribe_to(subribbit: Subribbit, user_id: str):
    sub = Subscription(subribbit=subribbit.name, user=user_id)
    sub.save()


############
# Subsribe #
############


def test_subscribe(client: FalconClient, sub1: Subribbit, user_id: str):
    response = client.simulate_post(
        f"/subribbits/v1/s/{sub1.name}",
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert Subscription.get(sub1.name, user_id) is not None


def test_subscribe_not_found(client: FalconClient, user_id: str):
    response = client.simulate_post(
        "/subribbits/v1/s/sub1",
        extras=user_env(user_id)
    )

    assert response.status_code == 404


def test_subscribe_already_subscribed(client: FalconClient, sub1: Subribbit, user_id: str):
    """ No-op when already subscribed"""
    subscribe_to(sub1, user_id)

    response = client.simulate_post(
        f"/subribbits/v1/s/{sub1.name}",
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert Subscription.is_subscribed(sub1.name, user_id)


def test_subscribe_to_private(client: FalconClient, private: Subribbit, user_id: str):
    """ You can't subscribe to a private subribbit.
        You can only be added to it by the creator
    """
    response = client.simulate_post(
        f"/subribbits/v1/s/{private.name}",
        extras=user_env(user_id)
    )
    assert response.status_code == 403


def test_subscribe_to_own_subribbit(client: FalconClient, sub1: Subribbit):
    """You're already subscribed to a subribbit you created.  Just smile and wave"""
    response = client.simulate_post(
        f"/subribbits/v1/s/{sub1.name}",
        extras=user_env(sub1.creator)
    )

    assert response.status_code == 200
    assert Subscription.is_subscribed(sub1.name, sub1.creator)


###############
# Unsubscribe #
###############


def test_unsubscribe(client: FalconClient, sub1: Subribbit, user_id: str):
    subscribe_to(sub1, user_id)

    response = client.simulate_delete(
        f"/subribbits/v1/s/{sub1.name}",
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert not Subscription.is_subscribed(sub1.name, user_id)


def test_unsubscribe_from_private(client: FalconClient, private: Subribbit, user_id: str):
    """You can unsubscribe yourself from a private subribbit"""
    subscribe_to(private, user_id)

    response = client.simulate_delete(
        f"/subribbits/v1/s/{private.name}",
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert not Subscription.is_subscribed(private.name, user_id)


def test_unsubscribe_not_subscribed(client: FalconClient, sub1: Subribbit, user_id: str):
    """ No-op when not subscribed"""
    response = client.simulate_delete(
        f"/subribbits/v1/s/{sub1.name}",
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert not Subscription.is_subscribed(sub1.name, user_id)


def test_unsubscribe_not_found(client: FalconClient, user_id: str):
    response = client.simulate_delete(
        f"/subribbits/v1/s/sub123",
        extras=user_env(user_id)
    )

    assert response.status_code == 404


def test_unsubscribe_from_own_subribbit(client: FalconClient, sub1: Subribbit):
    """You can't unsubscribe from a subribbit you created"""
    response = client.simulate_delete(
        f"/subribbits/v1/s/{sub1.name}",
        extras=user_env(sub1.creator)
    )

    assert response.status_code == 400
    assert Subscription.is_subscribed(sub1.name, sub1.creator)


#####################
# Get Subscriptions #
#####################


def test_get_subscriptions_none(client: FalconClient, user_id: str):
    response = client.simulate_get(
        "/subribbits/v1/s",
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == []


def test_get_subscriptions(
        client: FalconClient,
        sub1: Subribbit,
        sub2: Subribbit,
        private: Subribbit,
        user_id: str
):
    assert sub1  # just making intellij not complain about unused fixture
    subscribe_to(sub2, user_id)
    subscribe_to(private, user_id)

    response = client.simulate_get(
        "/subribbits/v1/s",
        extras=user_env(user_id)
    )

    assert response.status_code == 200
    assert response.json == [
        dict(
            name=sub2.name,
            creator=sub2.creator,
            created="2019-01-02T00:00:00+00:00",
            private=False
        ),
        dict(
            name=private.name,
            creator=private.creator,
            created="2019-01-03T00:00:00+00:00",
            private=True
        )
    ]
