import pytest
from falcon.testing import TestClient as FalconClient
from isodate import parse_datetime
from moto import mock_dynamodb2

from subribbits import APP
from subribbits.v1.subribbit import Subribbit
from subribbits.v1.subscription import Subscription


@pytest.fixture(name="user_id")
def user_id_fixture() -> str:
    return "user123"


@pytest.fixture(name='client')
def client_fixture() -> FalconClient:
    with mock_dynamodb2():
        Subribbit.create_table(read_capacity_units=1, write_capacity_units=1)
        Subscription.create_table(read_capacity_units=1, write_capacity_units=1)
        yield FalconClient(APP)


@pytest.fixture(name='sub1')
def sub1_fixture() -> Subribbit:
    sub = Subribbit(
        name='sub1',
        creator='user1',
        created=parse_datetime("2019-01-01T00:00:00Z")
    )
    sub.save()
    return sub


@pytest.fixture(name='sub2')
def sub2_fixture(user_id: str) -> Subribbit:
    sub = Subribbit(
        name='sub2',
        creator='user1',
        created=parse_datetime("2019-01-02T00:00:00Z")
    )
    sub.save()
    return sub


@pytest.fixture(name='private')
def private_fixture() -> Subribbit:
    sub = Subribbit(
        name='private_sub',
        creator='user1',
        created=parse_datetime("2019-01-03T00:00:00Z"),
        private=True
    )
    sub.save()
    return sub


def user_env(user_id: str) -> dict:
    return {"awsgi.event": {"requestContext": {"authorizer": {"principalId": user_id}}}}

################
# Make Private #
################


def test_make_private_not_found(client: FalconClient, user_id: str):
    response = client.simulate_patch(
        f"/subribbits/v1/r/sub1",
        json=dict(private=True),
        extras=user_env(user_id)
    )

    assert response.status_code == 404


def test_make_private_not_owner(client: FalconClient, sub1: Subribbit, user_id: str):
    response = client.simulate_patch(
        f"/subribbits/v1/r/{sub1.name}",
        json=dict(private=True),
        extras=user_env(user_id)
    )

    assert response.status_code == 403
    assert not Subribbit.get(sub1.name).private


def test_make_private(client: FalconClient, sub1: Subribbit, user_id: str):
    response = client.simulate_patch(
        f"/subribbits/v1/r/{sub1.name}",
        json=dict(private=True),
        extras=user_env(sub1.creator)
    )

    assert response.status_code == 200
    assert response.json == dict(
        name=sub1.name,
        creator=sub1.creator,
        created="2019-01-01T00:00:00+00:00",
        private=True
    )
    assert Subribbit.get(sub1.name).private


###############
# Make Public #
###############


def test_make_public_not_found(client: FalconClient, user_id: str):
    response = client.simulate_patch(
        f"/subribbits/v1/r/sub1",
        json=dict(private=False),
        extras=user_env(user_id)
    )

    assert response.status_code == 404


def test_make_public_not_owner(client: FalconClient, private: Subribbit, user_id: str):
    response = client.simulate_patch(
        f"/subribbits/v1/r/{private.name}",
        json=dict(private=False),
        extras=user_env(user_id)
    )

    assert response.status_code == 403
    assert Subribbit.get(private.name).private


def test_make_public(client: FalconClient, private: Subribbit, user_id: str):
    response = client.simulate_patch(
        f"/subribbits/v1/r/{private.name}",
        json=dict(private=False),
        extras=user_env(private.creator)
    )

    assert response.status_code == 200
    assert response.json == dict(
        name=private.name,
        creator=private.creator,
        created="2019-01-03T00:00:00+00:00",
        private=False
    )
    assert not Subribbit.get(private.name).private


def test_make_public_bad_request(client: FalconClient, private: Subribbit, user_id: str):
    response = client.simulate_patch(
        f"/subribbits/v1/r/{private.name}",
        json=dict(is_private=False),
        extras=user_env(private.creator)
    )

    assert response.status_code == 400
    assert Subribbit.get(private.name).private
