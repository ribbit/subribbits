from marshmallow import Schema, fields, ValidationError, validate
import falcon
from falcon import Request, Response, API

from .subribbit import Subribbit
from subribbits.v1.subscription import Subscription


class SubribbitSchema(Schema):
    name = fields.String(required=True, validate=validate.Length(Subribbit.MIN_LENGTH))
    creator = fields.String(dump_only=True)
    created = fields.DateTime(dump_only=True, format='iso8601')
    private = fields.Boolean(dump_only=True)


class SubribbitsResource(object):

    schema = SubribbitSchema()

    def on_get(self, request: Request, response: Response):
        """search"""
        if request.params.get("name"):
            names = request.params.get("name").split(",")
        else:
            names = []

        subribbits = list(Subribbit.batch_get(names))
        subscriptions = [sub.subribbit for sub in Subscription.users_index.query(request.context.user_id)]

        visible = filter(lambda s: not s.private or s.name in subscriptions, subribbits)

        response.media = self.schema.dump(visible, many=True)

    def on_post(self, request: Request, response: Response):
        """create"""
        try:
            name: str = self.schema.load(request.media)["name"]
        except ValidationError as e:
            raise falcon.HTTPBadRequest(str(e))

        for _ in Subribbit.query(name):
            raise falcon.HTTPBadRequest('subribbit already exists')

        subribbit = Subribbit(name=name, creator=request.context.user_id)
        subribbit.save()
        Subscription(subribbit=name, user=request.context.user_id).save()

        response.media = self.schema.dump(subribbit)


class SubribbitByNameResource(object):

    class UpdateSchema(Schema):
        private = fields.Boolean(required=True)

    schema = SubribbitSchema()
    update_schema = UpdateSchema()

    def on_delete(self, request: Request, response: Response, name: str):
        """delete"""
        try:
            subribbit = Subribbit.get(name)
            if subribbit.creator != request.context.user_id:
                raise falcon.HTTPForbidden()
            subribbit.delete()
            response.media = self.schema.dump(subribbit)
        except Subribbit.DoesNotExist:
            raise falcon.HTTPNotFound()

    def on_patch(self, request: Request, response: Response, name: str):
        """update"""
        try:
            private: bool = self.update_schema.load(request.media)["private"]
        except ValidationError as e:
            raise falcon.HTTPBadRequest(str(e))

        try:
            subribbit = Subribbit.get(name)
            if subribbit.creator != request.context.user_id:
                raise falcon.HTTPForbidden()

            subribbit.private = private
            subribbit.save()

            response.media = self.schema.dump(subribbit)
        except Subribbit.DoesNotExist:
            raise falcon.HTTPNotFound()


class MySubscriptionsResource(object):
    """Handles your own user subscriptions"""

    schema = SubribbitSchema()

    def on_get(self, request: Request, response: Response):
        subscribed_names = [
            subscription.subribbit
            for subscription in Subscription.users_index.query(request.context.user_id)
        ]

        subribbits = list(Subribbit.batch_get(subscribed_names))

        response.media = self.schema.dump(subribbits, many=True)


class MySubribbitSubscriptionResource(object):
    """Handles your subscription on a subribbit"""

    @staticmethod
    def on_post(request: Request, _response: Response, name: str):
        """Subscribe yourself to a subribbit"""
        try:
            subribbit = Subribbit.get(name)
            if subribbit.private:
                raise falcon.HTTPForbidden()

            sub = Subscription(subribbit=name, user=request.context.user_id)
            sub.save()
        except Subribbit.DoesNotExist:
            raise falcon.HTTPNotFound()

    @staticmethod
    def on_delete(request: Request, _response: Response, name: str):
        """Unsubscribe yourself to a subribbit"""
        try:
            subribbit = Subribbit.get(name)
            if subribbit.creator == request.context.user_id:
                raise falcon.HTTPBadRequest()

            sub = Subscription.get(name, request.context.user_id)
            sub.delete()
        except Subribbit.DoesNotExist:
            raise falcon.HTTPNotFound()
        except Subscription.DoesNotExist:
            pass  # no-op


def create() -> API:
    class CORSComponent(object):
        def process_response(self, req, resp, resource, req_succeeded):
            resp.set_header('Access-Control-Allow-Origin', '*')

    class AuthComponent(object):
        def process_request(self, request: Request, response: Response):
            try:
                user_id = request.env["awsgi.event"]["requestContext"]["authorizer"]["principalId"]
                request.context.user_id = user_id
            except KeyError:
                raise falcon.HTTPUnauthorized()

    app = API(middleware=[CORSComponent(), AuthComponent()])
    app.add_route("/subribbits/v1/r", SubribbitsResource())
    app.add_route("/subribbits/v1/r/{name}", SubribbitByNameResource())
    app.add_route("/subribbits/v1/s", MySubscriptionsResource())
    app.add_route("/subribbits/v1/s/{name}", MySubribbitSubscriptionResource())
    return app
