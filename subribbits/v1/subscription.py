from datetime import datetime, timezone
from os import environ

from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from pynamodb.models import Model


class UserSubscriptionsIndex(GlobalSecondaryIndex):
    class Meta:
        index_name  = 'user-subscriptions'
        read_capacity_units = 2
        write_capacity_units = 1
        projection = AllProjection()

    user = UnicodeAttribute(hash_key=True)
    subribbit = UnicodeAttribute(range_key=True)


class Subscription(Model):

    class Meta:
        table_name = environ["SUBSCRIPTIONS_TABLE_NAME"]

    subribbit = UnicodeAttribute(hash_key=True)
    user = UnicodeAttribute(range_key=True)
    created = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow().replace(tzinfo=timezone.utc))
    users_index = UserSubscriptionsIndex()

    @classmethod
    def is_subscribed(cls, subribbit: str, user_id: str) -> bool:
        try:
            Subscription.get(subribbit, user_id)
            return True
        except Subscription.DoesNotExist:
            return False
