from os import environ
from datetime import datetime, timezone

from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute, BooleanAttribute


class Subribbit(Model):

    MIN_LENGTH = 3

    class Meta:
        table_name = environ["SUBRIBBITS_TABLE_NAME"]

    name = UnicodeAttribute(hash_key=True)
    creator = UnicodeAttribute(null=False)
    created = UTCDateTimeAttribute(null=False, default=lambda: datetime.utcnow().replace(tzinfo=timezone.utc))
    private = BooleanAttribute(default=False)
