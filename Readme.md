ribbit-subribbits
=================

Service for creating subribbits and holding metadata

Resources
---------

- `v2`
    - GET: lists all subribbits
    - POST: create a subribbit
- `/v2/{name}`
    - GET: get subribbit metadata
    - DELETE: remove a subribbit